from dictionary import Dictionary
import sys

class Newsgroups:

    def __init__(self, filename):
        self.LABLES = { 0 : "sci.space", 1 : "comp.sys.ibm.pc.hardware", 2 : "rec.sport.basebell", 3 : "comp.windows.x", 4 : "talk.politics.misc", 5 : "misc.forsale",
                        6 : "rec.sport.hockey", 7 : "comp.graphics" }
        self.EMAIL_WORDS = {}
        self.LABEL_ORDERING = []
   #     self.WORD_SPAM = Dictionary()
        self.LABEL_WORDS = [ Dictionary() for x in range(0,8) ]
 #       self.WORD_HAM = Dictionary()
 #       self.NUM_SPAM = 0
 #       self.NUM_HAM = 0
        self.NUM_IN_LABEL = [ 0 for x in range(0,8) ]
        self.PROB_LABEL = [0 for x in range(0,8) ]     
        self.LIKELIHOOD_SPAM = Dictionary()
        self.LIKELIHOOD_HAM = Dictionary()
        self.LABEL_LIKELIHOOD = [ Dictionary() for x in range(0,8) ]
        self.VOCABULARY = Dictionary()

        email_input = open(filename).read().splitlines()
        index = 0
        for line in email_input:
            label = line.split()[0]
            words = line.split(" ")[1:]
            words = dict([(x.split(":")[0], int(x.split(":")[1])) for x in words])
            self.EMAIL_WORDS[index] = words.keys()
            label = int(label) 
            self.LABEL_ORDERING.append(label)
            self.NUM_IN_LABEL[label] += 1
            self.LABEL_WORDS[label].update(words)

#            if la is 1:
#                self.TEST_SPAM.append(1)
#                self.NUM_SPAM += 1
#                self.WORD_SPAM.update(words)
#            else:
#                self.TEST_SPAM.append(0)
#                self.NUM_HAM += 1
#                self.WORD_HAM.update(words)
            index+= 1
#        print [self.LABEL_WORDS[i].curr_dict.keys() for i in range(0,8)]
 #       print [self.NUM_IN_LABEL[i] for i in range(0,8)]
    
        for i in self.LABEL_WORDS:
            self.VOCABULARY.update(i.curr_dict)
#        print self.VOCABULARY.curr_dict.keys() 
#        self.VOCABULARY.update(self.WORD_SPAM.curr_dict)
#        self.VOCABULARY.update(self.WORD_HAM.curr_dict)

#        num_words_spam = self.WORD_SPAM.num_total_words()
#        num_words_ham = self.WORD_HAM.num_total_words()
        num_words_labels = [self.LABEL_WORDS[i].num_total_words() for i in range(0,8)]

        for word in self.VOCABULARY.curr_dict:
            for i in range(0,8):
                l = self.new_likelihood(word, i, num_words_labels[i])
                self.LABEL_LIKELIHOOD[i].update( {word : l} )
        print "made it"
 #           l = self.likelihood(word, True, num_words_spam)
 #           self.LIKELIHOOD_SPAM.update( {word: l} )
 #           l = self.likelihood(word, False, num_words_ham)
 #           self.LIKELIHOOD_HAM.update( {word: l} )
        
        for i in range(0,8):
            self.PROB_LABEL[i] = float(self.NUM_IN_LABEL[i]) / (sum(self.NUM_IN_LABEL))
#        self.PROB_SPAM = float(self.NUM_SPAM) / (self.NUM_SPAM + self.NUM_HAM)
#        self.PROB_HAM = float(self.NUM_HAM) / (self.NUM_HAM + self.NUM_SPAM)


    def new_likelihood(self, word, label, num_words):
        vocab = self.VOCABULARY.num_non_zero()
        count = self.LABEL_WORDS[label].occurrence(word)
        return float((count + 1)) / float(num_words + vocab)

    def likelihood(self, word, spam, num_words):
        vocab = self.VOCABULARY.num_non_zero()
        if spam:
            count = self.WORD_SPAM.occurrence(word)
        else:
            count = self.WORD_HAM.occurrence(word)
        return float((count + 1)) / float(num_words + vocab)
