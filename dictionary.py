class Dictionary:

    def __init__(self):
        self.curr_dict = {}

    def update(self, new_dictionary, shit=False):
        for key in new_dictionary:
            if key in self.curr_dict:
                self.curr_dict[key] += new_dictionary[key]
                if shit:
                    print "SHIT!"
            else:
                self.curr_dict[key] = new_dictionary[key]

    def occurrence(self, word):
        return self.curr_dict.get(word, 0)

    def num_non_zero(self):
        occur = self.curr_dict.values()
        occur = filter(lambda x: x != 0, occur)
        return len(occur)

    def num_total_words(self):
        return sum( self.curr_dict.values() )
