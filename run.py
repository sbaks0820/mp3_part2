#!/usr/bin/python
from Emails import Emails
from Newsletters import Newsgroups

import sys
from math import log

train_data = sys.argv[1]
test_data = sys.argv[2]
part = sys.argv[3]

if int(part) is 1:
    train_emails = Emails(train_data) 
    test_emails = Emails(test_data)
elif int(part) is 2:
    train_emails = Newsgroups(train_data) 
    test_emails = Newsgroups(test_data)

right = 0
wrong = 0

if int(part) is 1:
    for email in test_emails.EMAIL_WORDS.keys():
        spam_message = 0
        ham_message = 0 
        for word in test_emails.EMAIL_WORDS.get(email):
            spam_get = train_emails.LIKELIHOOD_SPAM.curr_dict.get(word)
            ham_get = train_emails.LIKELIHOOD_HAM.curr_dict.get(word)
            if spam_get is None or ham_get is None or spam_get < 0.0000001 or ham_get < 0.000001:
                continue
#        if not spam_get is None and not spam_get is 0:
#            spam_message = spam_message * train_emails.LIKELIHOOD_SPAM.curr_dict[word]
            print spam_get, ham_get
            spam_message += log(spam_get)
#        if not ham_get is None and not ham_get is 0:
#            ham_message = ham_message * train_emails.LIKELIHOOD_HAM.curr_dict[word]
            ham_message += log(ham_get)
#    print email, spam_message, ham_message

        spam_message = spam_message * train_emails.PROB_SPAM
        ham_message = ham_message * train_emails.PROB_HAM

#    print email, spam_message, ham_message, train_emails.PROB_SPAM, train_emails.PROB_HAM

        if spam_message > ham_message:
            if test_emails.TEST_SPAM[email] == 1:
                right += 1
                print "1 correct"
            else:
                wrong += 1
                print "1"
           
        else:
            if test_emails.TEST_SPAM[email] == 0:
                right += 1
                print "0 correct"
            else:
                wrong += 1
                print "0"
elif int(part) is 2:
    for email in test_emails.EMAIL_WORDS:
        prob = [0 for i in range(0,8)]
        for word in test_emails.EMAIL_WORDS.get(email):
            label_prob = [0 for x in range(0,8)]
            fucked = False
            for i in range(0,8):
                like = train_emails.LABEL_LIKELIHOOD[i].curr_dict.get(word)
                if like is None or like < 0.00000001:
                    fucked = True
                    break;
                label_prob[i] += log(like)
            if fucked:
                continue
            max_label = 0
            max_prob = label_prob[0]
            for i in range(0,8):
                if label_prob[i] > max_prob:
                    max_label = i
                    max_prob = label_prob[i]
            if max_label == test_emails.LABEL_ORDERING[email]:
                right += 1                
                print max_label, "correct"
            else:
                wrong += 1
                print max_label
print test_emails.EMAIL_WORDS.keys()
print len(test_emails.EMAIL_WORDS.keys())
print (float(right) / (right + wrong)) * 100 
