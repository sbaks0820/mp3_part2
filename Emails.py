from dictionary import Dictionary

class Emails:

    def __init__(self, filename):
        self.EMAIL_WORDS = {}
        self.TEST_SPAM = []
        self.WORD_SPAM = Dictionary()
        self.WORD_HAM = Dictionary()
        self.NUM_SPAM = 0
        self.NUM_HAM = 0
        self.LIKELIHOOD_SPAM = Dictionary()
        self.LIKELIHOOD_HAM = Dictionary()
        self.VOCABULARY = Dictionary()

        email_input = open(filename).read().splitlines()
        index = 0
        for line in email_input:
            spam = line.split()[0]
            words = line.split(" ")[1:]
            words = dict([(x.split(":")[0], int(x.split(":")[1])) for x in words])
            self.EMAIL_WORDS[index] = words.keys()
            if int(spam) is 1:
                self.TEST_SPAM.append(1)
                self.NUM_SPAM += 1
                self.WORD_SPAM.update(words)
            else:
                self.TEST_SPAM.append(0)
                self.NUM_HAM += 1
                self.WORD_HAM.update(words)
            index+= 1
    
        self.VOCABULARY.update(self.WORD_SPAM.curr_dict)
        self.VOCABULARY.update(self.WORD_HAM.curr_dict)

        num_words_spam = self.WORD_SPAM.num_total_words()
        num_words_ham = self.WORD_HAM.num_total_words()
        
        for word in self.VOCABULARY.curr_dict.keys():
            l = self.likelihood(word, True, num_words_spam)
            self.LIKELIHOOD_SPAM.update( {word: l} )
            l = self.likelihood(word, False, num_words_ham)
            self.LIKELIHOOD_HAM.update( {word: l} )

        self.PROB_SPAM = float(self.NUM_SPAM) / (self.NUM_SPAM + self.NUM_HAM)
        self.PROB_HAM = float(self.NUM_HAM) / (self.NUM_HAM + self.NUM_SPAM)

    def likelihood(self, word, spam, num_words):
        vocab = self.VOCABULARY.num_non_zero()
        if spam:
            count = self.WORD_SPAM.occurrence(word)
        else:
            count = self.WORD_HAM.occurrence(word)
        return float((count + 1)) / float(num_words + vocab)

